
package shoppingcart;
public class PaymentServiceFactory {
    public static PaymentServiceFactory paymentServiceFactory;
    public PaymentServiceFactory() {}
    public static PaymentServiceFactory getInstance(){
    if (paymentServiceFactory == null){
    paymentServiceFactory = new PaymentServiceFactory();
        }
    return paymentServiceFactory;
    }
   
    public PaymentService getPaymentService(PaymentServiceType type){
    
    PaymentService service = null;
    
    switch(type)
    {
        case CREDIT: service= new CreditPaymentService();
        
        break;
        
        case DEBIT: service = new DebitPaymentService();
        break; 
    }
    
    return service; 
    }
    
   
}