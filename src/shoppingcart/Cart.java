
package shoppingcart;
import java.util.ArrayList;
import java.util.List;

public class Cart {
    private List <Product> products;
    private PaymentService service;
    private Discount discount;
    
    
    public Cart(){
    products = new ArrayList<Product>();
    }
    
    public void setPaymentService(PaymentService service) {
    this.service=service; 
    
    }
    
    public void setDiscount(Discount discount) {
    this.discount=discount; 
    
    }
    
    public void addProduct(Product product){
        
    products.add(product);
    }
    
    
    public void payCart(){
        
        double totalPrice = 0; 
    for (Product p: products){
        totalPrice += p.getPrice(); 
    }
    
    service.processPayment(totalPrice);
    }
    
    public void applydiscount(){
    
    double totalPurchase=1.0;
    for (Product r: products){
    totalPurchase = r.getPrice() ;
    }
    discount.processDiscount(totalPurchase);
    }
}
