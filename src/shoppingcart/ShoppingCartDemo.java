
package shoppingcart;
public class ShoppingCartDemo {
    public static void main(String[] args) {
        PaymentServiceFactory factory = PaymentServiceFactory.getInstance();
        PaymentService creditService = factory.getPaymentService(PaymentServiceType.CREDIT);
        PaymentService debitService = factory.getPaymentService(PaymentServiceType.DEBIT);
        
        DiscountFactory discountAmount = DiscountFactory.getInstance();
        Discount discountByAmount = discountAmount.getDiscount(DiscountType.AMOUNT);
        Discount discountByPercentage = discountAmount.getDiscount(DiscountType.PERCENTAGE);
        
        Cart cart = new Cart();
        cart.addProduct(new Product ("shirt", 50, "small"));
        cart.addProduct(new Product ("pants", 80, "medium"));
        cart.setPaymentService(creditService);
        cart.payCart();
        cart.setPaymentService(debitService);
        cart.payCart();
        cart.setDiscount(discountByAmount);
        cart.applydiscount();
        cart.setDiscount(discountByPercentage);
        cart.applydiscount();
        
    }
  }
