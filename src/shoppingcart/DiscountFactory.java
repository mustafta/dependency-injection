/*
 * Taghreed Safaryan
 *  991494905
 * SYST10199 - Web Programming
 */
package shoppingcart;
public class DiscountFactory {
    
   public static DiscountFactory discountFactory;

    public DiscountFactory() {
    }
    
    public static DiscountFactory getInstance(){
    
    if (discountFactory == null){
    discountFactory = new DiscountFactory();
        }
    return discountFactory;
    }
   
    
    public Discount getDiscount(DiscountType type){
        
        Discount discount = null;
        switch(type){
        
        case AMOUNT: discount= new DiscountByAmount();
        break;
        
        case PERCENTAGE: discount = new DiscountByPercentage();
        break; 
        
    }
        return discount;
}
}
    
