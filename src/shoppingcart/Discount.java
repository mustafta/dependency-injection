/*
 * Taghreed Safaryan
 *  991494905
 * SYST10199 - Web Programming
 */
package shoppingcart;
public abstract class Discount {
    protected Discount(){}
    
    
    public abstract void processDiscount(double amount); 
     
    
}
