/*
 * Taghreed Safaryan
 *  991494905
 * SYST10199 - Web Programming
 */
package shoppingcart;
public enum DiscountType {
    AMOUNT,
    PERCENTAGE;    
}
